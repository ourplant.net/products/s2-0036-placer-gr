Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/01_operating_manual/S2-0036_A1_Betriebsanleitung.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/01_operating_manual/S2-0036_A_Operating%20Manual.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/02_assembly_drawing/s2-0036-B_ZNB_placer_gh-r.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/03_circuit_diagram/S2-0036_Greiferkopf_GH-R-C.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/05_spare_parts/S2-0036_A_Wartungsanweisungen.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/04_maintenance_instructions/S2-0036_A_Maintenance_instructions.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/05_spare_parts/S2-0036_A_EVL_Placer.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0036-placer-gr/-/raw/main/05_spare_parts/S2-0036_A_SWP_Placer.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
